//npm run test -- -i -u
import React from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { View } from "react-native";
import { create, act } from "react-test-renderer";
import App from "../App";
import { Provider } from "react-redux";
import store from "../store";
import rootReducer from "../src/redux/reducers/rootReducer";
import productReducer from "../src/redux/reducers/productReducer";
import { FIRST_COLOR, SECOND_COLOR } from "@env";

jest.spyOn(console, "warn").mockImplementation(() => {});
jest.spyOn(console, "error").mockImplementation(() => {});

fetch = jest.fn(() => Promise.resolve());

it("checks if Async Storage is used", () => {
	asyncOperationOnAsyncStorage = async () => {
		await AsyncStorage.setItem("LANG", "en");
		expect(AsyncStorage.getItem).toBeCalledWith("LANG");
	};
});

const tree = create(
	<Provider store={store}>
		<App />
	</Provider>,
);

describe("App", () => {
	it("App should render properly", () => {
		act(() => {
			expect(tree).toMatchSnapshot();
		});
	});
});

describe("productReducer", () => {
	it("productReducer => initial state", () => {
		expect(rootReducer(productReducer, {})).toMatchSnapshot();
	});
	it("productReducer => GET_ARTISANS", () => {
		act(() => {
			expect(rootReducer(productReducer, { type: "GET_ARTISANS" })).toMatchSnapshot();
		});
	});
	it("productReducer => GET_CLAIMS", () => {
		act(() => {
			expect(rootReducer(productReducer, { type: "GET_CLAIMS" })).toMatchSnapshot();
		});
	});
	it("productReducer => GET_PERKS", () => {
		act(() => {
			expect(rootReducer(productReducer, { type: "GET_PERKS" })).toMatchSnapshot();
		});
	});
	it("productReducer => GET_PRODUCTS", () => {
		act(() => {
			expect(rootReducer(productReducer, { type: "GET_PRODUCTS" })).toMatchSnapshot();
		});
	});
	it("productReducer => GET_REALTIES", () => {
		act(() => {
			expect(rootReducer(productReducer, { type: "GET_REALTIES" })).toMatchSnapshot();
		});
	});
});
