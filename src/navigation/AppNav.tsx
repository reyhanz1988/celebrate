import React, { useContext } from "react";
import { LoginContext, LangContext } from "../../App";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "../screens/LoginScreen";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { HomeNav, MenuNav } from "./AppStack";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import languages from "../screensTranslations/Navigation";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const AppNav = () => {
	const { token } = useContext(LoginContext);
	const { currentLang } = useContext(LangContext);
	if (token == null) {
		return (
			<Stack.Navigator screenOptions={{ headerShown: false }}>
				<Stack.Screen name="Login" component={LoginScreen} />
			</Stack.Navigator>
		);
	} else {
		return (
			<Tab.Navigator
				initialRouteName="HomeNav"
				screenOptions={{
					tabBarActiveTintColor: FIRST_COLOR,
					tabBarInactiveTintColor: "#666",
					headerShown: false,
					tabBarItemStyle: {
						backgroundColor: SECOND_COLOR,
						marginBottom: 5,
					},
					tabBarStyle: [{ display: "flex", height: 60 }, null],
				}}>
				<Tab.Screen
					name="HomeNav"
					component={HomeNav}
					options={{
						tabBarTestID: "HomeNav",
						tabBarLabel: languages[0][currentLang],
						tabBarLabelStyle: { fontSize: 12 },
						tabBarIcon: ({ color }) => (
							<MaterialCommunityIcons name="home" color={color} size={24} />
						),
					}}
				/>
				<Tab.Screen
					name="MenuNav"
					component={MenuNav}
					options={{
						tabBarTestID: "MenuNav",
						tabBarLabel: languages[1][currentLang],
						tabBarLabelStyle: { fontSize: 12 },
						tabBarIcon: ({ color }) => (
							<MaterialCommunityIcons name="menu" color={color} size={24} />
						),
					}}
				/>
			</Tab.Navigator>
		);
	}
};
export default AppNav;
