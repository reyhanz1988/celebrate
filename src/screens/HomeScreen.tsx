import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as accountActions from "../redux/actions/accountActions";
import * as productActions from "../redux/actions/productActions";
import { Dimensions, Image, StyleSheet, ScrollView, View, Text } from "react-native";
import { ActivityIndicator, Appbar } from "react-native-paper";
import * as ImagePicker from "react-native-image-picker";
import LangComponent from "../components/LangComponent";
import languages from "../screensTranslations/Home";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import PropTypes from "prop-types";

const HomeScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [loading, setLoading] = useState(true);
	const [response, setResponse] = React.useState(null);
	const [getDataError, setGetDataError] = useState([]);
	const [user, setUser] = useState([]);
	const [pics, setPics] = useState([]);
	const includeExtra = true;
	useEffect(() => {
		setLoading(true);
		props.getUser();
		props.getPics();
	}, []);
	//PROPS UPDATE USER
	useEffect(() => {
		setUser(props.getUserRes);
		setLoading(false);
	}, [props.getUserRes]);
	//PROPS UPDATE PACKETS
	useEffect(() => {
		console.log("getPicsRes", props.getPicsRes);
		setPics(props.getPicsRes);
		setGetDataError("");
		setLoading(false);
	}, [props.getPicsRes]);
	const onButtonPress = React.useCallback((type, options) => {
		if (type === "capture") {
			ImagePicker.launchCamera(options, setResponse);
		} else {
			ImagePicker.launchImageLibrary(options, setResponse);
		}
	}, []);

	const picsList = [];
	if (pics.length > 0) {
		for (const i in pics) {
			picsList.push(<Image style={styles.picsList} source={pics[i].uri} />);
		}
	}

	if (loading && loading == true) {
		//LOADING
		return (
			<View style={styles.wrapper} testID="HomeLoading">
				<ActivityIndicator size="large" />
			</View>
		);
	} else if (getDataError) {
		//ERROR HANDLING
		return (
			<View style={styles.wrapper} testID="HomeError">
				<ActivityIndicator size="large" />
			</View>
		);
	} else {
		//RENDER
		return (
			<View style={styles.wrapper} testID="HomeScreen">
				<Appbar.Header>
					<Appbar.Action
						icon="camera"
						onPress={() =>
							onButtonPress("capture", {
								saveToPhotos: true,
								mediaType: "photo",
								includeBase64: false,
								includeExtra,
							})
						}
					/>
					<Appbar.Action
						icon="plus"
						onPress={() =>
							onButtonPress("gallery", {
								selectionLimit: 0,
								mediaType: "photo",
								includeBase64: false,
								includeExtra,
							})
						}
					/>
					<Appbar.Content
						titleStyle={{ color: "#fff", textAlign: "center" }}
						title={languages[0][currentLang]}
					/>
					<LangComponent />
				</Appbar.Header>
				<ScrollView>
					<View style={styles.picsView}>{picsList}</View>
					<Text style={styles.text}>{JSON.stringify(response, null, 2)}</Text>
				</ScrollView>
			</View>
		);
	}
};

HomeScreen.propTypes = {
	getUser: PropTypes.func,
	getUserRes: PropTypes.any,
	getPics: PropTypes.func,
	getPicsRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};

type HomeScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	scrollWrapper: {
		marginBottom: 20,
	},
	picsView: {
		flex: 1,
		flexDirection: "row",
		flexWrap: "wrap",
	},
	picsList: {
		width: Dimensions.get("window").width * 0.325,
		height: Dimensions.get("window").width * 0.325,
		resizeMode: "cover",
		margin: Dimensions.get("window").width * 0.004,
	},
});

function mapStateToProps(props) {
	return {
		getUserRes: props.accountReducer.getUserRes,
		getPicsRes: props.productReducer.getPicsRes,
	};
}
const mapDispatchToProps = {
	...accountActions,
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
