import AccountApi from "../api/AccountApi";
import { createAsyncActionType/*, errorToastMessage*/ } from "../../components/util";

// checkLogin
export const CHECK_LOGIN = createAsyncActionType("CHECK_LOGIN");
export const checkLogin = (vars) => (dispatch) => {
	dispatch({
		type: CHECK_LOGIN.REQUESTED,
	});
	const response = AccountApi.checkLogin(vars);
	dispatch({
		type: CHECK_LOGIN.SUCCESS,
		payload: { response },
	});
	dispatch({
		type: LOGOUT_REDUX.SUCCESS,
		payload: {},
	});
	/*dispatch({
		type: CHECK_LOGIN.REQUESTED,
	});
	AccountApi.checkLogin(vars)
		.then((response) => {
			dispatch({
				type: CHECK_LOGIN.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: CHECK_LOGIN.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};

// getUser
export const GET_USER = createAsyncActionType("GET_USER");
export const getUser = () => (dispatch) => {
	dispatch({
		type: GET_USER.REQUESTED,
	});
	const response = AccountApi.getUser();
	dispatch({
		type: GET_USER.SUCCESS,
		payload: { response },
	});
	/*dispatch({
		type: GET_USER.REQUESTED,
	});
	AccountApi.getUser(vars)
		.then((response) => {
			dispatch({
				type: GET_USER.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_USER.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};

// logoutRedux
export const LOGOUT_REDUX = createAsyncActionType("LOGOUT_REDUX");
export const logoutRedux = (vars) => (dispatch) => {
	dispatch({
		type: LOGOUT_REDUX.REQUESTED,
	});
	const response = AccountApi.logoutRedux(vars);
	dispatch({
		type: LOGOUT_REDUX.SUCCESS,
		payload: { response },
	});
	dispatch({
		type: CHECK_LOGIN.SUCCESS,
		payload: {},
	});
	/*dispatch({
		type: LOGOUT_REDUX.REQUESTED,
	});
	AccountApi.logoutRedux(vars)
		.then((response) => {
			dispatch({
				type: LOGOUT_REDUX.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: LOGOUT_REDUX.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};
