import ProductApi from "../api/ProductApi";
import {
	createAsyncActionType,
	//errorToastMessage,
} from "../../components/util";

// getPics
export const GET_PICS = createAsyncActionType("GET_PICS");
export const getPics = () => (dispatch) => {
	dispatch({
		type: GET_PICS.REQUESTED,
	});
	const response = ProductApi.getPics();
	dispatch({
		type: GET_PICS.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getPics()
		.then((response) => {
			dispatch({
				type: GET_PICS.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_PICS.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};