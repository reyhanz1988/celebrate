//https://www.fakexy.com/fake-address-generator-es
const Pics = [
	{
		id: 1,
		uri: require("../../assets/images/pics/1.jpg"),
	},
	{
		id: 2,
		uri: require("../../assets/images/pics/2.jpg"),
	},
	{
		id: 3,
		uri: require("../../assets/images/pics/3.jpg"),
	},
	{
		id: 4,
		uri: require("../../assets/images/pics/4.jpg"),
	},
	{
		id: 5,
		uri: require("../../assets/images/pics/5.jpg"),
	},
	
];

const User = {
	id: 1,
	photo: require("../../assets/images/rey.jpg"),
	name: "Reyhan Emir Affandie",
	nick: "Reyhan",
	email: "reyhanz1988@gmail.com",
	phone: "938 20 02 51",
	gender: "Male",
	dob: "1988-06-17",
	ssn: "847-10-9194",
	address: "Carrer de la Creu, 16",
	city: "Balsareny",
	province: "Barcelona",
	country: "Spain",
	zipcode: "08660",
	vtype: "vans",
	vplate: "BG70AYA",
};

export { Pics, User };
