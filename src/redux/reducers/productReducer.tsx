import { GET_PICS } from "../actions/productActions";

const initialState = {
	successMsg: "",
	errorMsg: "",
	getPicsRes: [],
};

const productReducer = (state = initialState, action) => {
	const { type, payload } = action;

	switch (type) {
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_PICS                                                                                                                             */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_PICS.REQUESTED:
			return { ...state };
		case GET_PICS.SUCCESS:
			return {
				...state,
				getPicsRes: payload.response,
				errorMsg: "",
			};
		case GET_PICS.ERROR:
			return { ...state, errorMsg: payload.error };
		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		default:
			return state;
	}
};

export default productReducer;
