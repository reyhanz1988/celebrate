module.exports = [
	/*0*/
	{
		en: "Home",
		de: "Heim",
	},

	/*1*/
	{
		en: "Hello, below is the packets to deliver.",
		de: "Hallo, unten sind die zu liefernden Pakete.",
	},

	/*2*/
	{
		en: "Details",
		de: "Einzelheiten",
	},

	/*3*/
	{
		en: "Deliver",
		de: "Liefern",
	},
];
