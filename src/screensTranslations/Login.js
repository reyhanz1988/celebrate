module.exports = [
	/*0*/
	{
		en: "Language",
		de: "Sprache",
	},

	/*1*/
	{
		en: "Email",
		de: "Email",
	},

	/*2*/
	{
		en: "Password",
		de: "Passwort",
	},

	/*3*/
	{
		en: "Login",
		de: "Anmeldung",
	},
];
